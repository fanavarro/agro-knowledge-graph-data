# agro-knowledge-graph-data
This repository contains a backup for an example knowledge graph about agronomy articles. The backup file was created by means of neo4j-admin dump (neo4j version 3.5.7). It should be loaded through neo4j admin load.
